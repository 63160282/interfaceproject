/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfacepj;

/**
 *
 * @author User
 */
public class Dog extends LandAnimal{

    public Dog() {
        super("Dog",4);
    }

    @Override
    public void eat() {
    }

    @Override
    public void speak() {
    }

    @Override
    public void sleep() {
    }

    @Override
    public void run() {
        System.out.println("Dog: run");
    }
    
}

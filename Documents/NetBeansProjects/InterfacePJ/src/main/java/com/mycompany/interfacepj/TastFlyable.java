/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfacepj;

/**
 *
 * @author User
 */
public class TastFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog();
        Car car = new Car();
        car.startEngine();
        
        
        Flyable[] flyables = {bat,plane};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane P = (Plane)f;
                P.startEngine();
                P.run();
            }
            f.fly();
        }
        Runable[] runables ={dog,plane,car};
        for(Runable r:runables){
            r.run();
        }
    }
}
